<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShipFormRequest;
use App\Models\Ship;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ShipController extends Controller
{
	public function index(): \Inertia\Response
	{
		return Inertia::render('Ship/ShipIndex', [
			'ships' => Ship::with('gallery')->get(),
		]);
    }

	public function edit($id): \Inertia\Response
	{
		return Inertia::render('Ship/ShipEdit', [
			'ship_id' => $id
		]);
	}

	public function create(): \Inertia\Response
	{
		return Inertia::render('Ship/ShipEdit', [
			'ship_id' => null
		]);
	}

	public function upsert(ShipFormRequest $request)
	{
		$validated = $request->validated();

		$ship = $request->id ?
			Ship::find($request->id)->update($validated) :
			Ship::create($validated);

		return redirect('/ship/' . $request->id ?? $ship->id);
	}

	public function delete(Request $request)
	{
		Ship::find($request->id)->delete();
	}

	public function getData(Request $request): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Builder|array|null
	{
		return Ship::with(['gallery', 'cabins'])->find($request->id);
	}
}
