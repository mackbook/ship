<?php

namespace App\Http\Controllers;

use App\Http\Requests\CabinFormRequest;
use App\Models\CabinCategory;
use App\Models\Ship;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CabinCategoryController extends Controller
{

	public function edit($id): \Inertia\Response
	{
		return Inertia::render('Cabin/CabinEdit', [
			'cabin_id' => $id,
		]);
	}
	public function getData(Request $request)
	{
		return [
			'cabin' => CabinCategory::find($request->id),
			'ships' => Ship::select(['id','title'])->get()
		];
	}

	public function create(): \Inertia\Response
	{
		return Inertia::render('Cabin/CabinEdit', [
			'cabin_id' => null
		]);
	}

	public function upsert(CabinFormRequest $request)
	{
		$validated = $request->validated();

		$cabin = $request->id ?
			CabinCategory::find($request->id)->update($validated) :
			CabinCategory::create($validated);

		return redirect('/cabin/' . $request->id ?? $cabin->id);
	}

	public function delete(Request $request)
	{
		CabinCategory::find($request->id)->delete();
	}
}
