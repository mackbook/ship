<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    use HasFactory;

	protected $guarded = [];
	public $timestamps = false;
	protected $casts = [
		'spec' => 'json'
	];

	public function gallery(): \Illuminate\Database\Eloquent\Relations\HasMany
	{
		return $this->hasMany(Gallery::class);
	}

	public function cabins(): \Illuminate\Database\Eloquent\Relations\HasMany
	{
		return $this->hasMany(CabinCategory::class);
	}
}
