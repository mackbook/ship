<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CabinCategory extends Model
{
    use HasFactory;

	protected $guarded = [];
	public $timestamps = false;
	protected $casts = [
		'photos' => 'array'
	];
}
