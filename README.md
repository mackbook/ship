## Тестовое задание Измайлов Андрей
andrew.izmaylov@gmail.com
+7-977-812-8596

1. Install repository and generate key
2. Run 'cp .env.example .env'
3. Run `composer install`
4. Run `npm run dev`
5. Seed DB from sql dump file
6. Run `php artisan serve`
7. Check the basic CRUD interface


