<?php

use App\Http\Controllers\CabinCategoryController;
use App\Http\Controllers\ShipController;
use App\Models\Ship;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});


Route::post('/ship_details', [ShipController::class, 'getData'])->name('ship_details');
Route::post('/cabin_details', [CabinCategoryController::class, 'getData'])->name('cabin_details');
Route::get('/ships_list', function() {
	return Ship::select(['id', 'title'])->get();
})->name('ships_list');


Route::get('/ships', [ShipController::class, 'index']);
Route::get('/ship/{id}', [ShipController::class, 'edit']);
Route::get('/ship_new', [ShipController::class, 'create'])->name('ship.create');
Route::post('/ship', [ShipController::class, 'upsert'])->name('ship.upsert');
Route::post('/ship_delete', [ShipController::class, 'delete'])->name('ship.delete');

Route::post('/cabin', [CabinCategoryController::class, 'upsert'])->name('cabin.upsert');
Route::get('/cabin/{id}', [CabinCategoryController::class, 'edit'])->name('cabin.edit');
Route::get('/cabin_new', [CabinCategoryController::class, 'create'])->name('cabin.create');
Route::post('/cabin_delete', [CabinCategoryController::class, 'delete'])->name('cabin.delete');
