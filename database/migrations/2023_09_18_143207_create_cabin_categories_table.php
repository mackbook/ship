<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cabin_categories', function (Blueprint $table) {
            $table->id();
            $table->char('vendor_code', 10);
            $table->string('title');
            $table->text('description');
            $table->enum('type', ['Inside','Ocean view','Balcony','Suite'])->nullable();
			$table->json('photos')->nullable();
	        $table->unsignedBigInteger('ordering');
	        $table->unsignedTinyInteger('state');
	        $table->unsignedBigInteger('ship_id');
	        $table->foreign('ship_id')->references('id')->on('ships');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cabin_categories');
    }
};
